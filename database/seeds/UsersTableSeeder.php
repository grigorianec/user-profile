<?php


use App\Entity\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	public function run(): void
	{
		factory(User::class, 10)->create()->each(function(User $user) {
			$user->children()->saveMany(factory(User::class, 30)->create()->each(function(User $user) {
				$user->children()->saveMany(factory(User::class, 10)->create()->each(function (User $user){
					$user->children()->saveMany(factory(User::class, 6)->create()->each(function (User $user){
						$user->children()->saveMany(factory(User::class, 2)->create()->each(function (User $user){
						}));
					}));
				}));
			}));
		});
	}

}

