@extends('layouts.app')

@section('content')
    <div>
        <a href="{{ route('admin.users.create') }}" class="btn btn-success">
            Create User
        </a>
    </div>

        <div id="table_data">
             @include('admin.users.pagination_data')
        </div>

@endsection