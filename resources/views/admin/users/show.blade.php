@extends('layouts.app')

@section('content')
    <div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary mr-1">Edit</a>
        <form method="POST" action="{{ route('admin.users.destroy', $user) }}" class="mr-1">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger">Delete</button>
        </form>
    </div>
    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th>
                Photo
            </th>
            <td>
                @if($user->photo !== null)
                     <img width="150px" height="150px" src="{{$user->photo }}" alt="photo">
                @else
                    <img width="150px" height="150px" src="/build/images/user.png" alt="photo">
                @endif
            </td>
        </tr>
        <tr>
            <th>ID</th><td>{{ $user->id }}</td>
        </tr>
        <tr>
            <th>Full Name</th><td>{{ $user->full_name }}</td>
        </tr>
        <tr>
            <th>Email</th><td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Position</th><td>{{ $user->position }}</td>
        </tr>
        <tr>
            <th>Salary</th><td>{{ $user->salary }}</td>
        </tr>
        <tr>
            <th>Slug</th><td>{{ $user->slug }}</td>
        </tr>
        <tr>
            <th>First date</th><td>{{ $user->created_at }}</td>
        </tr>
        <tbody>
        </tbody>
    </table>

    <div>
        <a href="{{ route('admin.users.create', $user) }}" class="btn btn-success">
            Create User
        </a>
    </div>

    <h2 class="text-center">
        Children
    </h2>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Slug</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($users as $user)
            <tr>
                <td><a href="{{ route('admin.users.show', $user) }}">{{ $user->full_name }}</a></td>
                <td>{{ $user->slug }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@endsection