/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});


// $(document).ready(function(){
//
//     $(document).on('click', '.pagination li a', function(event){
//         event.preventDefault();
//         var page = $(this).attr('href').split('page=')[1];
//         fetch_data(page);
//     });
//
//     function fetch_data(page)
//     {
//         $.ajax({
//             url:"/admin/users/fetch_data?page="+page,
//             success:function(data)
//             {
//                 $('#table_data').html(data);
//             }
//
//             // error: function (xhr, ajaxOptions, thrownError) {
//             //     alert(xhr.status);
//             //     alert(xhr.responseText);
//             //     alert(thrownError);
//             // }
//         });
//     }
//
// });

// $(document).ready(function(){
//
//     function clear_icon()
//     {
//         $('#id_icon').html('');
//         $('#full_name_icon').html('');
//     }
//
//     function fetch_data(page, sort_type, sort_by, query)
//     {
//         $.ajax({
//             url:"/admin/users/fetch_data?page="+page+"&sortby="+sort_by+"&sorttype="+sort_type+"&query="+query,
//             success:function(data)
//             {
//                 // $('tbody').html('');
//                 $('#table_data').html(data)
//             },
//             error: function (xhr, ajaxOptions, thrownError) {
//                 alert(xhr.status);
//                 alert(xhr.responseText);
//                 alert(thrownError);
//             }
//         })
//     }
//
//     $(document).on('click', '#start-search', function(){
//         var query = $('#search').val();
//         var column_name = $('#hidden_column_name').val();
//         var sort_type = $('#hidden_sort_type').val();
//         var page = $('#hidden_page').val();
//         fetch_data(page, sort_type, column_name, query);
//     });
//
//     $(document).on('click', '.sorting', function(){
//         var column_name = $(this).data('column_name');
//         var order_type = $(this).data('sorting_type');
//         var reverse_order = '';
//         if(order_type == 'asc')
//         {
//             $(this).data('sorting_type', 'desc');
//             reverse_order = 'desc';
//             clear_icon();
//             $('#'+column_name+'_icon').html('<i class="fas fa-angle-down"></i>');
//         }
//         if(order_type == 'desc')
//         {
//             $(this).data('sorting_type', 'asc');
//             reverse_order = 'asc';
//             clear_icon
//             $('#'+column_name+'_icon').html('<span class="fas fa-angle-up"></span>');
//         }
//         $('#hidden_column_name').val(column_name);
//         $('#hidden_sort_type').val(reverse_order);
//         var page = $('#hidden_page').val();
//         var query = $('#search').val();
//         fetch_data(page, reverse_order, column_name, query);
//     });
//
//     $(document).on('click', '.pagination a', function(event){
//         event.preventDefault();
//         var page = $(this).attr('href').split('page=')[1];
//         var column_name = $('#hidden_column_name').val();
//         var sort_type = $('#hidden_sort_type').val();
//
//         var query = $('#search').val();
//
//         $('li').removeClass('active');
//         $(this).parent().addClass('active');
//         fetch_data(page, sort_type, column_name, query);
//     });
//
// });
