<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class AddTreeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('users', function (Blueprint $table) {
	    	$table->string('position');
	    	$table->string('salary')->default(' ');
		    $table->string('slug')->default(' ');
		    $table->unique(['parent_id', 'slug']);
		    $table->unique(['parent_id', 'full_name']);
		    NestedSet::columns($table);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('users', function (Blueprint $table) {
		    $table->dropColumn('position');
		    $table->dropColumn('salary');
		    $table->dropColumn('slug');
		    NestedSet::dropColumns($table);
	    });
    }
}
