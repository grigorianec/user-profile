@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ route('admin.users.update', $user) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="photo" class="col-form-label">Photo</label>
            <input id="photo" type="file" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" value="{{ old('photo', $user->photo) }}">
            @if ($errors->has('photo'))
                <span class="invalid-feedback"><strong>{{ $errors->first('photo') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="full_name" class="col-form-label">Full Name</label>
            <input id="full_name" class="form-control{{ $errors->has('full_name') ? ' is-invalid' : '' }}" name="full_name" value="{{ old('full_name', $user->full_name) }}" required>
            @if ($errors->has('full_name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('full_name') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="email" class="col-form-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>
            @if ($errors->has('email'))
                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="slug" class="col-form-label">Slug</label>
            <input id="slug" type="text" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{ old('slug', $user->slug) }}" required>
            @if ($errors->has('slug'))
                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="salary" class="col-form-label">Salary</label>
            <input id="salary" type="text" class="form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}" name="salary" value="{{ old('salary', $user->salary) }}" required>
            @if ($errors->has('salary'))
                <span class="invalid-feedback"><strong>{{ $errors->first('salary') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="position" class="col-form-label">Position</label>
            <input id="position" type="text" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position" value="{{ old('position', $user->position) }}" required>
            @if ($errors->has('position'))
                <span class="invalid-feedback"><strong>{{ $errors->first('position') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection