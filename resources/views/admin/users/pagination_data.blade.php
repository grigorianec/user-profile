<div class="col-md-3">
    <div class="form-group">
        <label for="search" class="col-form-label">Search</label>
        <input type="text" name="search" id="search" class="form-control" />
    </div>
    <div class="mb-2">
        <button id="start-search" class="btn btn-primary">
            Search
        </button>
    </div>
</div>
<div class="table-responsive">
    <div id="table-date">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th class="sorting" data-sorting_type="asc" data-column_name="id" style="cursor: pointer">ID <span id="id_icon"></span></th>
                <th>Photo</th>
                <th class="sorting" data-sorting_type="asc" data-column_name="full_name" style="cursor: pointer">Full Name <span id="full_name_icon"></span></th>
                <th class="sorting" data-sorting_type="asc" data-column_name="position" style="cursor: pointer">Position<span id="position_icon"></span></th>
                <th class="sorting" data-sorting_type="asc" data-column_name="salary" style="cursor: pointer">Salary<span id="salary_icon"></span></th>
            </tr>
            </thead>
            <tbody>

            @foreach ($users as $user)
                <tr>
                    <td>
                        {{ $user->id }}
                    </td>
                    <td>
                        @for ($i = 0; $i < $user->depth; $i++)
                        &mdash;
                        @endfor
                        @if($user->photo !== null)
                            <img width="50px" height="50px" src="{{$user->photo }}" alt="photo">
                        @else
                            <img width="50px" height="50px" src="/build/images/user.png" alt="photo">
                        @endif
                    </td>
                    <td>

                        <a href="{{ route('admin.users.show', $user) }}">
                            {{ $user->full_name }}
                        </a>
                    </td>
                    <td>
                        {{ $user-> position }}
                    </td>
                    <td>
                        {{ $user-> salary }} $
                    </td>
                </tr>
            @endforeach
            {{ $users->links() }}
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
</div>
<input type="hidden" name="hidden_page" id="hidden_page" value="1" />
<input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id" />
<input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
<script>
    $(document).ready(function(){

        function clear_icon()
        {
            $('#id_icon').html('');
            $('#full_name_icon').html('');
        }

        function fetch_data(page, sort_type, sort_by, query)
        {
            $.ajax({
                url:"/admin/users/fetch_data?page="+page+"&sortby="+sort_by+"&sorttype="+sort_type+"&query="+query,
                success:function(data)
                {
                    // $('tbody').html('');
                    $('#table_data').html(data)
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(xhr.responseText);
                    alert(thrownError);
                }
            })
        }

        $(document).on('click', '#start-search', function(){
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page = $('#hidden_page').val();
            fetch_data(page, sort_type, column_name, query);
        });

        $(document).on('click', '.sorting', function(){
            var column_name = $(this).data('column_name');
            var order_type = $(this).data('sorting_type');
            var reverse_order = '';
            if(order_type == 'asc')
            {
                $(this).data('sorting_type', 'desc');
                reverse_order = 'desc';
                clear_icon();
                $('#'+column_name+'_icon').html('<i class="fas fa-angle-down"></i>');
            }
            if(order_type == 'desc')
            {
                $(this).data('sorting_type', 'asc');
                reverse_order = 'asc';
                clear_icon
                $('#'+column_name+'_icon').html('<span class="fas fa-angle-up"></span>');
            }
            $('#hidden_column_name').val(column_name);
            $('#hidden_sort_type').val(reverse_order);
            var page = $('#hidden_page').val();
            var query = $('#search').val();
            fetch_data(page, reverse_order, column_name, query);
        });

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();

            var query = $('#search').val();

            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(page, sort_type, column_name, query);
        });

    });
</script>

