@extends('layouts.app')

@section('content')

    <div class="mb-2">
        <a href="{{ route('admin.users.create') }}" class="btn btn-success">
            Create User
        </a>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Photo</th>
            <th>Name</th>
            <th>Email</th>
            <th>Position</th>
            <th>Salary</th>
            <th>First Date</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($users as $user)
            <tr>
                <td>
                    {{ $user->id }}
                </td>
                <td>
                    @for ($i = 0; $i < $user->depth; $i++)
                    &mdash;
                    @endfor
                    @if($user->photo !== null)
                        <img width="50px" height="50px" src="{{$user->photo }}" alt="photo">
                    @else
                        <img width="50px" height="50px" src="/build/images/user.png" alt="photo">
                    @endif
                </td>
                <td>
                    <a href="{{ route('admin.users.show', $user) }}">
                        {{ $user->full_name }}
                    </a>
                </td>
                <td>
                    {{ $user->email}}
                </td>
                <td>
                    {{ $user-> position }}
                </td>
                <td>
                    {{ $user-> salary }} $
                </td>
                <td>
                    {{ $user->created_at->format('d/m/Y')}}
                </td>
            </tr>
        @endforeach
        {{ $users->links() }}
        </tbody>
    </table>
    {{ $users->links() }}

@endsection