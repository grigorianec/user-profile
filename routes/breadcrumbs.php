<?php

use App\Entity\User;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;


Breadcrumbs::register('home', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('home'));
});

Breadcrumbs::register('login', function (Crumbs $crumbs) {
	$crumbs->parent('home');
	$crumbs->push('Login', route('login'));
});

Breadcrumbs::register('register', function (Crumbs $crumbs) {
	$crumbs->parent('home');
	$crumbs->push('Register', route('register'));
});

Breadcrumbs::register('reset', function (Crumbs $crumbs) {
	$crumbs->parent('home');
	$crumbs->parent('login');
	$crumbs->push('Register', route('reset'));
});

Breadcrumbs::register('admin.home', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('admin.home'));
});

Breadcrumbs::register('admin.users.index', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('admin.home'));
	$crumbs->push('AllUsers', route('admin.users.index'));
});

Breadcrumbs::register('admin.users.create', function (Crumbs $crumbs) {
	$crumbs->parent('admin.users.index');
	$crumbs->push('Create', route('admin.users.create'));
});

Breadcrumbs::register('admin.users.show', function (Crumbs $crumbs, User $user) {
	if ($parent = $user->parent) {
		$crumbs->parent('admin.users.show', $parent);
	} else {
		$crumbs->parent('admin.users.index');
	}
	$crumbs->push($user->full_name, route('admin.users.show', $user));
});

Breadcrumbs::register('admin.users.edit', function (Crumbs $crumbs, User $user) {
	$crumbs->parent('admin.users.show', $user);
	$crumbs->push('Edit', route('admin.users.edit', $user));
});

Breadcrumbs::register('admin.users.paginate', function (Crumbs $crumbs) {
	$crumbs->parent('admin.home');
	$crumbs->push('AjaxUsers', route('admin.users.paginate'));
});

