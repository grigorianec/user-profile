<?php


namespace App\Http\Controllers;


use App\Entity\User;
use Exception;
use Illuminate\Http\RedirectResponse;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Throwable;

class UserController extends Controller
{
	public function index()
	{
		$users = User::defaultOrder()->withDepth()->paginate(30);

		return view('admin.users.index', compact('users'));
	}

	public function paginate()
	{
		$users = User::defaultOrder()->withDepth()->paginate(30);

		return view('admin.users.paginate', compact('users'));
	}

	/**
	 * @param Request $request
	 *
	 * @return array|string
	 * @throws Throwable
	 */
	function fetch_data(Request $request)
	{
		$sort_by = $request->get('sortby');
		$sort_type = $request->get('sorttype');
		$query = $request->get('query');
		if($request->ajax())
		{
			$users = User::with('ancestors')
			             ->withDepth()
		                 ->where('id', 'like', '%'.$query.'%')
				         ->orWhere('full_name', 'like', '%'.$query.'%')
				         ->orWhere('email', 'like', '%'.$query.'%')
				         ->orderBy($sort_by, $sort_type)
			             ->paginate(30);
			return view('admin.users.pagination_data', compact('users'))->render();
		}

		return view('admin.users.paginate', compact('users'));
	}

	public function create(User $user)
	{
		$parent = $user->getParentId();

		return view('admin.users.create', compact('parent'));
	}

	/**
	 * @param Request $request
	 *
	 * @return RedirectResponse
	 * @throws ValidationException
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'full_name' => 'required|string|max:255||unique:users',
			'email' => 'required|string|max:255|unique:users',
			'slug' => 'required|string|max:255|unique:users',
			'position' => 'required|string|max:255',
			'salary' => 'required|string|max:255',
			'password' => 'required|string|min:8'
		]);

		$user = User::create([
			'full_name' => $request['full_name'],
			'email' => $request['email'],
			'slug' => $request['slug'],
			'position' => $request['position'],
			'salary' => $request['salary'],
			'parent_id' => $request['parent'],
			'password' => Hash::make($request['password']),
		]);



		return redirect()->route('admin.users.show', $user);
	}

	public function show(User $user)
	{
		$users = User::where('parent_id', $user->id)->orderBy('full_name')->get();

		return view('admin.users.show', compact('user','users'));
	}

	public function edit(User $user)
	{
		return view('admin.users.edit', compact('user'));
	}

	/**
	 * @param User $user
	 * @param Request $request
	 *
	 * @return RedirectResponse
	 * @throws ValidationException
	 */
	public function update(User $user, Request $request)
	{
		$this->validate($request, [
			'photo' => 'nullable|image|mimes:jpg,png|max:5048',
			'full_name' => 'required|string|max:255',
			'email' => 'required|string|max:255',
			'slug' => 'required|string|max:255',
			'position' => 'required|string|max:255',
			'salary' => 'required|string|max:255',
		]);

		$path = null;
		$image = $request->file('photo');
		if ($request['photo'] !== null){
			$new_name = rand() . '.' . $image->getClientOriginalExtension();
			$image->move(public_path('build/images'), $new_name);
			$path = '/build/images/' . $new_name;
			$user->update(['photo' => $path]);
		}


		$user->update([
			'name' => $request['name'],
			'email' => $request['email'],
			'slug' => $request['slug'],
			'position' => $request['position'],
			'salary' => $request['salary']
		]);



		return redirect()->route('admin.users.show', $user);
	}

	public function first(User $user)
	{
		if ($first = $user->siblings()->defaultOrder()->first()) {
			$user->insertBeforeNode($first);
		}

		return redirect()->route('admin.users.index');
	}

	public function up(User $user)
	{
		$user->up();

		return redirect()->route('admin.users.index');
	}

	public function down(User $user)
	{
		$user->down();

		return redirect()->route('admin.users.index');
	}

	public function last(User $user)
	{
		if ($last = $user->siblings()->defaultOrder('desc')->first()) {
			$user->insertAfterNode($last);
		}

		return redirect()->route('admin.users.index');
	}
	/**
	 * @param User $user
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 */
	public function destroy(User $user)
	{
		$user->delete();

		return redirect()->route('admin.users.index');
	}
}