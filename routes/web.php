<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get( '/', 'HomeController@index' )->name( 'home' );

Route::group(
	[
		'prefix' => 'admin',
		'as' => 'admin.',
		'middleware' => ['auth'],
	],
	function () {

		Route::get( '/', 'Admin\HomeController@index' )->name( 'home' );

		Route::group([
			'prefix' => 'users',
			'as' => 'users.'
		],
		function(){
			Route::get('/paginate', 'UserController@paginate')->name('paginate');
			Route::get('/fetch_data', 'UserController@fetch_data');
			Route::get('/', 'UserController@index')->name('index');
			Route::get('/create/{user?}', 'UserController@create')->name('create');
			Route::post('/store/{user?}', 'UserController@store')->name('store');
			Route::get('/{user}', 'UserController@show')->name('show');
			Route::delete('/users/{user}/destroy', 'UserController@destroy')->name('destroy');
			Route::get('/{user}/edit', 'UserController@edit')->name('edit');
			Route::put('/{user}/update', 'UserController@update')->name('update');

			Route::group(['prefix' => '{user}'], function () {
				Route::post('/first', 'UserController@first')->name('first');
				Route::post('/up', 'UserController@up')->name('up');
				Route::post('/down', 'UserController@down')->name('down');
				Route::post('/last', 'UserController@last')->name('last');
			});
		});

	});
